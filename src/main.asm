# .text defines the instruction portion of the assembly code
.text
.align 4

# export the "main" entry point so it is visible to the linker
.global main

# main entry point label
main: 
	addi x1, x0, %lo(str)
	lui x2, 0xc0000
	lbu x3, 0(x1)
	beq x3, x0, +16
	sb x3, 0(x2)
	addi x1, x1, 1
	jal x0, -16
	jal x0, 0

.data
str:
	.asciz "Hello, World!"
