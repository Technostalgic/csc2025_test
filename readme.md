# Template Assembly VSCode Project - RISC-V in emulsiV simulator


## Setup

For the setup, I'll be using the Debian linux distribution running under WSL (windows subsystem for
linux). So this same procedure should also work for any one who uses Debian Linux.

1. Install Visual Studio Code (VSCode)

2. Open VSCode, click on the "extensions" icon in the left panel, and search for and install these 
extensions: "RISC-V", "Live Server"

3. Install GNU assembler toolchain for risc-v architecture. On Debian, with the apt package manager, 
the command is `sudo apt install gcc-riscv64-unknown-elf`. However it may differ if you're using a 
different distro that uses another package manager such as pacman. Despite what the package name 
seems to indicate, these assembler tools should work on both 32 bit and 64 bit machines.

4. Install Git version control software so you can clone this repository. 
command: `sudo apt install git`

5. Clone this repository: First, create a directory somewhere to store your project. Could be in
your documents or wherever else you want. I used `/home/tteck/test_proj` (tteck is my computer
user name). Now, open a terminal in that directory or `cd` to it from an already open terminal. 
Once you're in  terminal in that folder, clone the repo with the command 
`git clone https://gitlab.com/Technostalgic/csc2025_test`.

6. Open the project with VSCode: "File->Open Folder" then, inside the directory that you cloned the 
repository in, select the "csc2025_test" directory. The project explorer on the left side of the 
window should have a root level directorly labelled `.vscode`. This directory **must** be at root 
level, it should not be one directory deep, otherwise VSCode will not recognize the project 
configuration. If your directory has: "csc2025_test/.vscode" instead of just ".vscode", then you 
need to go back to "File->Opent Folder" open the "csc2025_test" directory directly.

7. Start the live server to run the assembly virtual environment in. Make sure you have the 
Live Server extension installed and enabled, if it is, you should see a little wireless icon in the
bottom right corner of the VSCode window (it should be in the blue status bar on the very right
and have a label next to it `Go Live`). Click that icon and it will launch the emulsiV 
RISC-V environment for your code to run in. After pressing that button, it should start and the text
should change into "Port:5500". If it says a different number, you can go into the extension 
settings and set the port to 5500 and then restart it, but you shouldn't need to, that is the 
default port.

8. Now you can compile/assemble and run the program! On the left side of the VSCode window there is
a Debug tab, select that, and then at the top of the left panel, press the green play button 
labelled "Launch Chrome against localhost". Or you can press the shortcut key f5. This will compile
the short "hello world" program I have written, copy the file into a specific directory so the 
emulsiV program runs it, and then it will launch chrome and navigate to localhost where the
emulsiV environment is running, load your program, and then run your program.

**Note** - you will have to have google chrome installed for this to work, since visual studio uses
chrome as the default browser for debugging frontend code, and the emulsiV environment runs as a
website frontend