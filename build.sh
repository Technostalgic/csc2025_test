rm -r ./target/*
riscv64-unknown-elf-gcc -march=rv32i -mabi=ilp32 -c -o ./target/startup.o ./src/startup.s
riscv64-unknown-elf-as -march=rv32i -mabi=ilp32 -c -o ./target/main.o ./src/main.asm
riscv64-unknown-elf-gcc -march=rv32i -mabi=ilp32 -nostdlib -T ./env/emulsiv.ld -o ./target/program.elf ./target/main.o
riscv64-unknown-elf-objcopy -O ihex ./target/program.elf ./target/program.hex
rm ./env/emulsiV/_run/program.hex
cp ./target/program.hex ./env/emulsiV/_run/program.hex
echo "TODO: finish build script"